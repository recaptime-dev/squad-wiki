# Recap Time Squad Wiki

[![Netlify Status](https://api.netlify.com/api/v1/badges/4b0bbe53-ba71-407d-bd17-601a8acaad9a/deploy-status?branch=main)](https://app.netlify.com/sites/recaptime-squad-wiki/deploys)

The new home for squad's organizational wiki and the handbook itself. If you're a squad member and you need the internal version, [sign in here](https://internal.squad.lorebooks.eu.org) or [learn more in handbook](https://squad.lorebooks.eu.org/go/internal-handbook-docs).

## Where it's been deployed

We use Netlify to host stuff, although we use GitLab CI to do the deployment itself.
The website is accessible at [`squad.lorebooks.eu.org`](https://squad.lorebooks.eu.org) while we're ironing out
the migration plan until further notice.[^1]

[^1]: We'll be working on the deployments itself soonish.
