title: General Onboarding
---

# Onboarding at Recap Time Squad

Welcome aboard to the squad, newbie! Since our organization is mostly "all remote" and our virtual headquarters is in our Matrix space for staff
(`#internal.recaptime.dev:envs.net`)

## Channel Support for new crewmates

Once your Matrix account is fully onboarded to our staff-only space, you should receive invites to these rooms from day 1 from your manager or one of our squad leaders:

* `#recaptimesquad-staff-intro` - Go say hi and introduce yourself.
* `#recaptimesquad-staff-questions` - Ask questions that the Handbook can't help with or if need clarifications.
* `#recaptimesquad-staff-intersectionality_hub` - Connect with other crewmates about things relating to intersectionality and
what we do in DIB (Diversity, Inclusion and Belonging).
* `#recaptimesquad-staff-it_support` - Any IT related support, such as issues with your squad email address, SSO, etc.
* `#recaptimesquad-staff-watercooler` - General chitchat for squad members.

## Managers of New Crewmates

TODO
