# Squad Handbook

The Recap Time Squad Handbook is the central repository for how we run the organization.
As an open-source organization/company, the handbook is open to the world, and we welcome feedback and patches.

Most of the handbook written here are based off and inspired by [GitLab Team Handbook](https://about.gitlab.com/handbook), with major modifications to adapt to our needs.

For internal version of the handbook, go to [our internal wiki](https://internal.squad.lorebooks.eu.org/handbook) and sign in with your [Internal SSO account](./company/internal-sso.md).


## Company/Organization

**General quick links**: [Subgroup README](https://mau.dev/RecapTime/squad/gitlab-profile/blob/main/README.md) | [Public Issue tracker](https://mau.dev/RecapTime/squad/issue-tracker/issues) - [sourcehut version](https://todo.sr.ht/~recaptime-dev/) - [Internal issue tracker](https://recaptime.jetbrains.space)

* [About the squad](/organization.md)
    * [History](/organization/history.md)
* [Handbook](/handbook/about.md)
    * [How to Use](/handbook/handbook-usage.md)
    * [Handbook Style Guide](/style-guide/handbook.md)
    * [CHANGELOGs for Handbook](/handbook/changelog.md)
* [Culture](/organization/culture)
    * [Community Code of Conduct](/community/code-of-conduct) - this applies even to squad members, in addition to regular org policies and BCoCE.

## Engineering and Open-source

* [Engineering](/handbook/engineering)
* [Open-source](/handbook/open-source)
    * [OSPO/Open-source Ops](/handbook/open-source/office)
    * [Legal Department and Open-source](handbook/legal/open-source) - Contributor Agreements, license reviews, etc.
* [DevSecOps / Platform Engineering](/handbook/devsecops)
    * [Security](/handbook/devsecops/security)
    * [Infrastructure](/handbook/infra)
* [IT Adminops](/handbook/it-adminops)

## Community

TODO

## Legal and Employee Relations

* [Legal](/handbook/legal)
    * [Employee Relations / Human Resources](/handbook/employee-relations)
    * [Business Code of Conduct and Ethics](/handbook/legal/coc) - The staff version of our Community Code of Conduct, but for official business conduct.
