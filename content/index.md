title: Home
---

# Welcome to Recap Time Squad Wiki!

!!! warning "Migration in progress"
    The old Miraheze wiki will be in process of archival and decommisioning until further notice.

The official wiki for everything about the brand, its members and the projects we do. Maintained as part of the Community Lores project, the documentation and wiki department of Recap Time Squad.

## News

_For archived news (more than 12 months OR when the entries go more than 5-10) please see the [news archive](./community/news-archive.md)._

## Getting started / Explore

TBD

## Connect with the crew + community

TBD

## Contribute

Like most of the projects we do, our public organizational wiki is open to anyone to improve upon. Since

* [Contributing](contribute/index.md) - learn how you can use your knowledge and time to help us maintain
projects and keep things tidy.
* [Donate](./community/donate.md) - A record of previous purchases and costs are publicly releases here,
alongside on Open Collective.

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoiZXh0ZW5zaW9uczpcbiAgcHJlc2V0Oi
BnZm1cbiAgZW1vamk6XG4gICAgc2hvcnRjdXRzOiB0cnVlXG4i
LCJoaXN0b3J5IjpbLTc5MDAwNTI3OF19
-->